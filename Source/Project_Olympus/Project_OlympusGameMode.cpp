// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Project_OlympusGameMode.h"
#include "Project_OlympusCharacter.h"
#include "UObject/ConstructorHelpers.h"

AProject_OlympusGameMode::AProject_OlympusGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
