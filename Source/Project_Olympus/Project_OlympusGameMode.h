// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Project_OlympusGameMode.generated.h"

UCLASS(minimalapi)
class AProject_OlympusGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AProject_OlympusGameMode();
};



