# Project_Olympus
***Experimental Third Person Shooter.***

**Assets**
*(Will be updated with additional assets later)*

1. Using TwinBlast and Murdock
2. Infiltrator Demo soldiers // REPLACED WITH SHOOTER PROJECT SOLDIER
3. Infiltrator Effects  // REPLACED WITH SHOOTER PROJECTS EFFECTS
4. Paragon effects for TwinBlast and Murdock.
5. UE4 Animation Starter Pack
6. Animations of TwinBlast and Murdock.
7. Paragon Minions and their associated effects and animation files.

**Objectives**
*(Changes will be made according to progress.)*

1. Intense action packed Third Person Shooter.
2. Stealth options, alternate pathways and objectives.
3. Procedural play areas.
4. *Cinematics*
